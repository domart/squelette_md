# squelette_md


## Impression "print do pdf" dans Firefox.

Dans firefox, pour effectuer des réglages pour l'impression dans un fichier PDF, il faut ouvrir un nouvel onglet, et copier :

```
about:config
```

dans la barre d'adresse, puis accepter le risque.

### Réglages entête et pied de pages

Les options suivantes permettent de faire les réglages de l'entête et du pied de pages (rechercher `print.printer_Mozilla_Save_to_PDF`) :
- `print.printer_Mozilla_Save_to_PDF.print_headerleft`
- `print.printer_Mozilla_Save_to_PDF.print_headercenter` : `&T`
- `print.printer_Mozilla_Save_to_PDF.print_headerright` : `2nde16`
- `print.printer_Mozilla_Save_to_PDF.print_footerleft`
- `print.printer_Mozilla_Save_to_PDF.print_footercenter` : `&PT`
- `print.printer_Mozilla_Save_to_PDF.print_footerright`  : `2022-2023`

Pour modifier les valeurs, il faut utiliser :
- Title => `&T` (placed in Header-Left by default)
- URL => `&U` (placed in Header-Right by default)
- Page number of total (x of y) => `&PT` (placed in Footer-Left by default)
- Page number (x) => `&P`
- Date/Time => `&D` (placed in Footer-Right by default)

### Décaler les entêtes et pieds de pages

Par défaut, les entêtes et pieds de pages en "print to pdf" sont collés au bord. Pour changer cela, il faut modifier les 4 clefs suivantes, en remplaçant 0 par 20 par exemple :

- `print.printer_Mozilla_Save_to_PDF.print_edge_bottom`
- `print.printer_Mozilla_Save_to_PDF.print_edge_left`
- `print.printer_Mozilla_Save_to_PDF.print_edge_right`
- `print.printer_Mozilla_Save_to_PDF.print_edge_top`


## Mermaid.js

De la doc pas mal ici [https://jojozhuang.github.io/tutorial/mermaid-cheat-sheet/](https://jojozhuang.github.io/tutorial/mermaid-cheat-sheet/).

La doc officielle : [https://mermaid.js.org/syntax/examples.html](https://mermaid.js.org/syntax/examples.html).