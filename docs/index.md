---
title: Chapitre I - Les admonitions
---
{{set_page_courante('docs/index.md')}}



# Chapitre I - Les admonitions

{{
ajoute_admonition(
admonition_type = 'exemple',
corps ='
    <iframe src="./h5p/eq_N1.html" width="948" height="296" frameborder="0" allowfullscreen="allowfullscreen" title="Test2"></iframe>
    <script src="https://e-education.recia.fr/moodle/mod/hvp/library/js/h5p-resizer.js" charset="UTF-8"></script>
'
)
}}

## Lien

- Dans le théorème {{ajoute_reference('pythagore')}}, on parlera de Pythagore.

- Dans le théorème {{ajoute_reference('thales')}}, on parlera de [Thalès](https://fr.wikipedia.org/wiki/Thal%C3%A8s){target=_blank}.

- Dans le propriété {{ajoute_reference('somme_arithmetique')}}, on parlera de la somme des termes d'une suite arithmétique.

## Les remarques

### Repliable ouverte

{{
    ajoute_admonition(admonition_type = 'remarque', corps = 'Repliable ouverte')
}}

### Repliable fermée

{{
    ajoute_admonition(admonition_type = 'remarques', corps = 'Repliable fermée', ouverte=False)
}}

### Non repliable

{{
    ajoute_admonition(admonition_type = 'remarques', corps = 'Non repliable', repliable=False)
}}


Dans ce chapitre, $\mathbb{N}$ désigne l'ensemble des entiers naturels.

## Les autres

### Les définitions

#### Sans titre

{{
ajoute_admonition(
admonition_type = 'definition',
corps = '
    Soit $(u_n)$ une suite géométrique de raison $q\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}, n\times{}m$,
    
    $$u_n = u_0\times{}q^n$$
')
}}

#### Avec un titre

{{
ajoute_admonition(
admonition_type = 'definition',
titre = 'avec titre',
corps = '
    Soit $(u_n)$ une suite géométrique de raison $q\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}$,
    
    $$u_n = u_0\\times{}q^n$$
')
}}

### Les théorèmes

#### De Pythagore

{{
ajoute_admonition(
admonition_type = "theoreme",
corps = '
    Le triangle $ABC$ est rectangle en $C$ si, et seulement si, $AB^2=AC^2+BC^2$.

    <center>
        <iframe scrolling="no" title="Pythagore" src="https://www.geogebra.org/material/iframe/id/pxwzce3g/width/350/height/250/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="350px" height="250px" style="border:0px;"> </iframe>
    </center>
',
titre = 'Pythagore',
libelle = 'pythagore'
)
}}

#### De Thalès

{{
ajoute_admonition(
admonition_type = 'theoreme',
corps = '
    Soit $ABC$ un triangle. Soient $D$ et $E$ deux points tels que $D\in(AB)$ et $E\in(AC)$.

    Si $(DE)//(BC)$, alors

    $$\dfrac{AD}{AB}=\dfrac{AE}{AC}$$

    <center>
        <iframe scrolling="no" title="Thalès" src="https://www.geogebra.org/material/iframe/id/q5abzm4t/width/700/height/500/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="500px" style="border:0px;"> </iframe>
    </center>
',
titre = 'Thalès',
libelle = 'thales'
)
}}



## Les suites

### Les suites arithémtiques

{{
ajoute_admonition(
admonition_type = 'definition',
titre = 'Suite arithmétique',
corps = '
    On dit que $(u_n)_{n\in\mathbb{N}}$ est **arithmétique** si

    $$\exists{}r\in\mathbb{N}, \forall{}n\in\mathbb{N}, u_{n+1}=u_{n}+r$$

    $r$ est appelée la **raison** de la suite.
')
}}

    
{{
ajoute_admonition(
admonition_type = 'theoreme',
corps =
    '
    Soit $r\in\mathbb{R}$.

    $(u_n)_{n\in\mathbb{N}}$ est arithmétique de raison $r$ si, et seulement si,

    $$\forall{}n\in\mathbb{N}, \boxed{u_n=u_0+n\times{}r}$$
    '
)
}}

{{
ajoute_admonition(
admonition_type = 'propriete',
corps = '

    Soit $(u_n)_{n\in\mathbb{N}}$ une suite arithmétique de raison $r\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}$ et pour tout $p\in\mathbb{R}$,

    $$u_n=u_p+\left(n-p\right)\times{}r$$

',
libelle = 'suite_arithmetique')
}}

{{
ajoute_admonition(
admonition_type = 'propriete',
corps = '

    Soit $(u_n)_{n\in\mathbb{N}}$ une suite arithmétique de raison $r\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}$,

    $$
    \begin{array}{rcl}
        \sum_{k=0}^{n}{u_k} & = & u_0\times\left(n+1\right)+r\dfrac{n(n+1)}{2} \\\\
                            & = & (n+1)\left(\dfrac{2u_0+r\times{}n}{2}\right)
    \end{array}
    $$

',
libelle = 'somme_arithmetique')
}}

    

```mermaid
graph LR
    A(($\Omega$)) -- $1$ --> B((Circle))
    A --> C(Round Rect)
    B --> D[[Rhombus]]
    C --> D
    D --> E{accolade simple}
    D --> F{{Doubles}}
    F --> G[Crochet]
```