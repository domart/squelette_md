---
title: Chapitre II - La suite
---
{{set_page_courante('docs/section 1/partie1.md')}}

# Chapitre II - La suite

## Intro

Ici, on parlera de suite (dans la définition {{ajoute_reference('geo')}}).

On parlera aussi de la remarque {{ajoute_reference('rmq')}}.

## Les trucs

{{
ajoute_admonition(
admonition_type = 'definition',
corps = '
    Soit $(u_n)$ une suite géométrique de raison $q\in\mathbb{R}$.

    Alors, pour tout $n\in\mathbb{N}, n\times{}m$,
    
    $$u_n = u_0\times{}q^n$$
',
libelle = 'geo')
}}



## Les remarques

### Repliable ouverte

{{
    ajoute_admonition(admonition_type = 'remarque', corps = 'Repliable ouverte')
}}

### Repliable fermée

{{
    ajoute_admonition(admonition_type = 'remarques', corps = 'Repliable fermée', ouverte=False)
}}

### Non repliable

{{
    ajoute_admonition(admonition_type = 'remarques', corps = 'Non repliable', repliable=False, libelle = 'rmq')
}}

## Un arbre de proba

{{
ajoute_admonition(
admonition_type = 'propriete',
corps = '
    ```mermaid
    graph LR
        A(($\Omega$)) -- 1/3 --> B_N((Noire))
        B_N -- 1 --> B_NR(Rouge)
        A -- 2/3 --> B_R(Rouge)
        B_R -- 1/2 --> B_RN((Noire))
        B_R -- 1/2 --> B_RR(Rouge)
    ```
'
)
}}


{{
ajoute_admonition(
admonition_type = 'propriete',
corps = '
    ```mermaid
    graph LR
        A(( )) -- 1/4 --> S{{Succès}}
        A      -- 3/4 --> E[Echec]
        S      -- 1/4 --> SS{{Succès}}
        S      -- 3/4 --> SE[Echec]
        E      -- 1/4 --> ES{{Succès}}
        E      -- 3/4 --> EE[Echec]
        SS     -- 1/4 --> SSS{{Succès}}
        SS     -- 3/4 --> SSE[Echec]
        SE     -- 1/4 --> SES{{Succès}}
        SE     -- 3/4 --> SEE[Echec]
        ES     -- 1/4 --> ESS{{Succès}}
        ES     -- 3/4 --> ESE[Echec]
        EE     -- 1/4 --> EES{{Succès}}
        EE     -- 3/4 --> EEE[Echec]
        SSS    ---->      SSSi>SSS]
        SSE    ---->      SSEi>SSE]
        SES    ---->      SESi>SES]
        SEE    ---->      SEEi>SEE]
        ESS    ---->      ESSi>ESS]
        ESE    ---->      ESEi>ESE]
        EES    ---->      EESi>EES]
        EEE    ---->      EEEi>EEE]
        linkStyle 14 stroke-width:0px;
        linkStyle 15 stroke-width:0px;
        linkStyle 16 stroke-width:0px;
        linkStyle 17 stroke-width:0px;
        linkStyle 18 stroke-width:0px;
        linkStyle 19 stroke-width:0px;
        linkStyle 20 stroke-width:0px;
        linkStyle 21 stroke-width:0px;
    ```
'
)
}}