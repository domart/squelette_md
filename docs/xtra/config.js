window.MathJax = {
    tex: {
        inlineMath: [['$', '$'], ["\\(", "\\)"]],
        displayMath: [['$$', '$$'], ["\\[", "\\]"]],
        processEnvironments: true
    },
    options: {
        ignoreHtmlClass: ".*|",
        processHtmlClass: "arithmatex"
    },
    svg: {
      fontCache: 'global'
    }
};

document$.subscribe(() => { 
    MathJax.typesetPromise()
})
