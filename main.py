debug = False
"""
Dictionnaire permettant de faire le lien entre le nom de la fonction appelée (la clef) et :
- le type de l'admonition (la valeur[0]),
- son libellé (la valeur[1]),
- le compteur associé (la valeur[2]).
"""
ADMONITION = {'definition': ['definition', 'Définition'],
              'theoreme'  : ['theoreme',   'Théorème'],
              'propriete' : ['propriete',  'Propriété'],
              'remarque'  : ['remarque',   'Remarque'],
              'remarques' : ['remarque',   'Remarques'],
              'exemple'   : ['exemple',    'Exemple'],
              'exemples'  : ['exemple',    'Exemples']}

"""
ADMONITIONS_AJOUTEES est un dictionnaire dont :
- la clef est l'emplacement de la page MD courante,
- la valeur est une liste de dictionnaire correspond chacun à une admonition ajoutée dans
    la page courante, dans l'ordre d'apparition dans cette page.
    Il peut y avoir jusqu'à trois informations :
    - Le type de l'admonition (obligatoire)   : clef `admonition_type`.
    - Le libellé de l'admonition (facultatif) : clef `libelle`, pour faire des références à cette admonition.
    - Le compteur associé (obligatoire)       : clef `compteur`.
"""
ADMONITIONS_AJOUTEES = {}

page_courante = ''

def define_env(env):

    def incremente_compteur(admonition_type: str) -> int:
        """
        Incrémente le compteur correspondant au type de l'admonition,
        et renvoie la nouvelle valeur de ce compteur.
        """
        if admonition_type in ['definition']:
            global definition_cpt
            definition_cpt += 1
            return definition_cpt
        elif admonition_type in ['theoreme']:
            global theoreme_cpt
            theoreme_cpt += 1
            return theoreme_cpt
        elif admonition_type in ['propriete']:
            global propriete_cpt
            propriete_cpt += 1
            return propriete_cpt
        elif admonition_type in ['remarque', 'remarques']:
            global remarque_cpt
            remarque_cpt += 1
            return remarque_cpt
        elif admonition_type in ['exemple', 'exemples']:
            global exemple_cpt
            exemple_cpt += 1
            return exemple_cpt
    
    def reinitialise_compteurs():
        """
        Réinitialise tous les compteurs à 0.
        """
        global definition_cpt
        definition_cpt = 0
        global theoreme_cpt
        theoreme_cpt = 0
        global propriete_cpt
        propriete_cpt = 0
        global remarque_cpt
        remarque_cpt = 0
        global exemple_cpt
        exemple_cpt = 0

    @env.macro
    def set_page_courante(page):
        """
        Permet d'enregistrer l'emplacement de la page en cours de construction.

        Est nécessaire pour les références qui sont faites avant la définition de l'objet :
        il faut alors parcourir la page md pour trouver le bon numéro de compteur.

        L'appel de cette fonction doit se faire tout au début de chaque page .md, en indiquant
        le chemin relatif depuis la racine du projet : 'docs/...'
        """
        global page_courante
        page_courante = page

        reinitialise_compteurs()

        # Contenu contient tout le contenu de la page MD, sans espace ni retour chariot,
        # et uniquement des simples quotes ''.
        contenu = ''
        with open(page, 'r') as page_md:
            for ligne in page_md:
                # On remplace les " par des '.
                contenu += ligne.replace('"', "'")
        while ('\n' in contenu or ' ' in contenu):
            contenu = contenu.replace('\n', '').replace(' ', '')
        
        ADMONITIONS_DE_LA_PAGE = []

        while 'ajoute_admonition(' in contenu:
            indice = contenu.index('ajoute_admonition(')
            contenu = contenu[indice + len('ajoute_admonition('):]
            sous_chaine = construit_sous_chaine(contenu)
            admonition_courant = {}
            
            for couple in sous_chaine.split(","):
                if ('admonition_type=' in couple or 'libelle=' in couple):
                    if '=' in couple:
                        [clef, valeur] = couple.split('=')
                    admonition_courant[clef] = valeur.replace("'", '')

            ADMONITIONS_DE_LA_PAGE.append(admonition_courant)
        for admonition in ADMONITIONS_DE_LA_PAGE:
            # Gestion du compteur.
            admonition['compteur'] = incremente_compteur(admonition['admonition_type'])
        reinitialise_compteurs()

        if debug:
            print(page_courante, ADMONITIONS_DE_LA_PAGE)
        
        ADMONITIONS_AJOUTEES[page_courante] = ADMONITIONS_DE_LA_PAGE
        
        return ''
    
    def construit_sous_chaine(chaine):
        """
        Sur la chaîne passée en paramère, permet de récupérer
        la chaîne de caractère correspondant aux arguments utilisés lors de l'appel
        courant de `ajoute_admonition`.
        """
        cpt = 0
        indice = 0
        sous_chaine = ''
        while cpt != 1:
            if chaine[indice] == '(':
                cpt -= 1
            elif chaine[indice] == ')':
                cpt += 1
            sous_chaine += chaine[indice]
            indice += 1
        return sous_chaine[:-1]
    
    @env.macro
    def ajoute_admonition(admonition_type: str, corps: str, titre: str = '', repliable: bool = True, ouverte: bool = True, libelle: str = '') -> str:
        """
        Crée le code markdown ajoutant l'admonition concerné.
        
        Arguments :
        - admonition_type : L'admonition à utiliser (info, tip, success, ... ou une perso : definition, theoreme, ...).
        - corps : Le corps de la définition.
        - titre : Le titre (optionnel) de la définition.
        - repliable : Indique si l'admonition est repliable (Vrai par défaut).
        - ouverte : Indique, pour un admonition repliable, si elle est ouverte (Vrai par défaut).
        - libelle : Y a-t-il un libellé (vide par défaut).
        """

        # On commence par incrémenter le compteur correspondant au type de l'admonition :
        compteur = incremente_compteur(admonition_type)

        result = ''
        
        # Si un libellé a été indiqué, on l'ajoute ici .
        if libelle != '':
            # Création de l'ancre.
            result += f'<a id="{libelle}"></a>\n'
        
        admonition_style = '!!!'
        if repliable:
            admonition_style = '???'
            if ouverte:
                admonition_style += '+'
        else:
            repliable_str = '!!!'
        if titre != '':
            titre = ' - ' + titre
        result += f'{admonition_style}{ADMONITION[admonition_type][0]} "{ADMONITION[admonition_type][1]} {compteur}{titre}"\n'
        result += '    ' + traite_chaine(corps)
        return result

    def traite_chaine(chaine: str) -> str:
        """
        Permet de ne pas avoir à doubler les anti-slashs sur \t.
        Sinon, ils sont remplacés par une tabulation, et \time{} par exemple est remplacé par "    ime".
        Idem pour les '\f', les '\b', les '\r'.
        """
        result = ''
        for caractere in chaine:
            if caractere == '\t':
                result += '\\t'
            elif caractere == '\f':
                result += '\\f'
            elif caractere == '\b':
                result += '\\b'
            elif caractere == '\r':
                result += '\\r'
            else:
                result += caractere
        return result
    
    @env.macro
    def ajoute_reference(libelle: str) -> str:
        """
        Permet d'ajouter une référence vers l'admonition dont le libellé
        est indiqué en paramètre.
        """
        for admonition in ADMONITIONS_AJOUTEES[page_courante]:
            if 'libelle' in admonition:
                if admonition['libelle'] == libelle:
                    return f'<a href="#{libelle}"><b>{admonition["compteur"]}</b></a>'
        return -1